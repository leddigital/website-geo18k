<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://geo18k.com.br">
        <img src="http://geo18k.com.br/web/images/geo18k_logo.png" height="30" alt="Logo-Geo18k">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $menuGeo; ?> <span class="caret"></span></a>
          
          <ul class="dropdown-menu">
            <li>
              <h3 style="color: #333 !important; "><a href="<?php echo $idioma; ?>-<?php echo $lnkMenuSobre; ?>">
              <?php echo $subMenuSobre; ?></a></h3>

              <p style="text-align: center;">
                <i class="fas fa-phone"></i> <a href="tel:551732338944">+55(17) 3233-8944</a>
              </p>
              
              <p style="text-align: center;">
                <i class="fab fa-whatsapp"></i> <a href="https://wa.me/5517991336990?text=Entre%20em%20contato">+55(17) 99133-6990</a>
              </p>
              
              <p style="text-align: center;">
                <i class="far fa-envelope"></i> <a href="mailto:atendimento@geo18k.com.br">atendimento@geo18k.com.br</a>
              </p>
            </li>
          </ul>
      </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $menuColecao; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">  
            <div class="row">
	            <div class="col-sm-12">
	            	<li>
	            		<div style="position: relative; z-index: 99">
	            			<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuLima; ?>">
	            			<img style="width: 100%;" src="http://geo18k.com.br/web/images/Vicenza.png" alt="Vicenza">
	            			<h3 style="position: absolute; bottom: 20px; left: 10px;"><?php echo $subMenuLima; ?></h3>
	            		</a>
	            	</div>
	            	</li>
	            </div>
	            <div class="col-sm-12">
	            	<li>
	            		<div style="position: relative; z-index: 99">
		            		<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuSelva; ?>">
		            		<img style="width: 100%;" src="http://geo18k.com.br/web/images/Selva.jpg" alt="Singapura"> 
		            		<h3 style="position: absolute; bottom: 20px; left: 10px;"><?php echo $subMenuSelva; ?></h3>
		            		</a>
	            		</div>
	            	</li>
	            </div>
            </div>
            
           <div class="row">
	            <div class="col-sm-12">
	            	<li>
	            		<div style="position: relative; z-index: 99">
		            		<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuDunas; ?>">
		            		<img style="width: 100%;" src="http://geo18k.com.br/web/images/Dunas.jpg" alt="Vicenza">
		            		<h3 style="position: absolute; bottom: 20px; left: 10px;"><?php echo $subMenuDunas; ?></h3>
		            		</a>
	           		 	</div>
	            	</li>
	            </div>
	            <div class="col-sm-12">
	            	<li>
						<div style="position: relative; z-index: 99">
		            		<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuRio; ?>">
		            		<img style="width: 100%;" src="http://geo18k.com.br/web/images//RJ.jpg" alt="Singapura">
		            		<h3 style="position: absolute; bottom: 20px; left: 10px;"><?php echo $subMenuRio; ?></h3>
		            		</a>
	            		</div>
	            	</li>
	            </div>
           </div>
            
            <div class="row">
            	<div class="col-sm-12">
	            	<li>
	            		<div style="position: relative; z-index: 99">
		            		<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuFleur; ?>">
		            		<img style="width: 100%;" src="http://geo18k.com.br/web/images/Fleur.jpg" alt="Vicenza">
		            		<h3 style="position: absolute; bottom: 20px; left: 10px;"><?php echo $subMenuFleur; ?></h3>
		            		</a>
	            		</div>
	            	</li>
	            </div>
	            <div class="col-sm-12">
	            	<li>
						<div style="position: relative; z-index: 99">
		            		<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuMosc; ?>">
		            		<img style="width: 100%;" src="http://geo18k.com.br/web/images/Moscow.jpg" alt="Singapura">
		            		<h3 style="position: absolute; bottom: 20px; left: 10px;"><?php echo $subMenuMosc; ?></h3>
		            		</a>
	            		</div>
	            	</li>
	            </div>
            </div>
            
	           <div class="row">
		           	<div class="col-sm-12">
		           	 <li>
		           	 	<div style="position: relative; z-index: 99">
			           	 	<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuSing; ?>">
			           	 	<img style="width: 100%;" src="http://geo18k.com.br/web/images/Singapura.jpg" alt="Fleur">
			           	 	<h3 style="position: absolute; bottom: 20px; left: 10px;"><?php echo $subMenuSing; ?></h3>
			           	 	</a>
		           		</div>
		           	</li>
		           </div>
	       		</div>
          </ul>
			<ul class="nav navbar-nav">
				<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $menuCatalago; ?> <span class="caret"></span></a>
					<ul class="dropdown-menu">
					 <li>
					<h4>
						<img src="http://geo18k.com.br/web/images/icon/anel-geo.png" alt="Anéis" width="100px">
						<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuAneis; ?>">
						<?php echo $subMenuAneis; ?></a>
					</h4>
					
					<h4>
						<img src="http://geo18k.com.br/web/images/icon/brincos-geo.png" alt="" width="100px">
						<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuBrincos; ?>">
						<?php echo $subMenuBrincos; ?></a>
					</h4>
				
					<h4>
						<img src="http://geo18k.com.br/web/images/icon/conjunto-geo.png" alt="" width="100px">
						<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuConjuntos; ?>">
						<?php echo $subMenuConjuntos; ?></a>
					</h4>
				
					<h4>
						<img src="http://geo18k.com.br/web/images/icon/colar-geo.png" alt="" width="100px">
						<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuGargantilhas; ?>">
						<?php echo $subMenuGargantilhas; ?></a>
					</h4>
					
					<h4>
						<img src="http://geo18k.com.br/web/images/icon/pingentes-geo.png" alt="" width="100px">
						<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuPingentes; ?>">
						<?php echo $subMenuPingentes; ?></a>
					</h4>
				
					<h4>
						<img src="http://geo18k.com.br/web/images/icon/pulseiras-geo.png" alt="" width="100px">
						<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuPulseiras; ?>">
						<?php echo $subMenuPulseiras; ?></a>
					</h4>
					</li>
				 </ul>
				</li>
				<li>
					<a href="<?php echo $idioma; ?>-<?php echo $lnkBlog; ?>">
					<?php echo $menuBlog; ?></a>
				</li>
				<li>
					<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuContato; ?>">
					<?php echo $subMenuContato; ?></a>
				</li>
				
			</ul>

			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $menuIdioma; ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
						 <li>
						 	<h4>
								<img src="http://geo18k.com.br/web/images/icon/pt.png" alt="">
								<a href="pt<?php echo $link; ?>">Português</a>
							</h4>
					
							<h4>
								<img src="http://geo18k.com.br/web/images/icon/en.png" alt="">
								<a href="en<?php echo $link; ?>">Engilsh</a>
							</h4>
				
							<h4>
								<img src="http://geo18k.com.br/web/images/icon/es.png" alt="">
								<a href="es<?php echo $link; ?>">Español</a>
							</h4>
						 </li>
			</li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>