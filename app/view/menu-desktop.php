			
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div id="nav-wrapper"> 
				<nav id="nav">
				
					<div class="effect-men">
						<!-- <li class="active"><a href="#">Início</a></li> -->
						
						<!-- IMG Logo Menu 
						<a href="/" class="logo-header"><li class="">
							<img src="<?php echo $imagem; ?>web/images/logo-fundo-transparente.png" class="img-responsive" alt="">
						</li></a>
						IMG Logo Menu -->	

						<span class="geo">
							<li>
								<h4 class=""><?php echo $menuGeo; ?></h4>
							</li>
							<figcaption class="geoF col-md-12">
								
								<!-- Espaçamento entre menus -->
								<div class="col-md-12">
									<div class="col-md-3">
										
									</div>
								<!-- Espaçamento entre menus -->

									<div class="col-md-3">
										<P><span class="fa fa-phone"> +55(17) 3233-8944</span></P>
										<P><span class="fa fa-whatsapp"> +55(17) 9 9133-6990</span></P>
										<p><span class="fa fa-envelope-o"> <a href="mailto:atendimento@geo18k.com.br" class="">
										atendimento@geo18k.com.br</a></span></p>
									</div>
									<div class="col-md-3">
										<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuSobre; ?>" class="">
											<h3><span class="fa fa-google"></span></h3>
												<h4>
													<span><?php echo $subMenuSobre; ?></span>
												</h4>
										</a>
										<!-- <a href="<?php echo $idioma; ?>-<?php echo $lnkMenuContato; ?>" class="">
											<h3><span class="fa fa-envelope"></span></h3>
											<h4>
												<span><?php echo $subMenuContato; ?></span>
											</h4>
										</a> -->
										
									</div>
									
									<!-- Espaçamento entre menus -->
									<div class="col-md-3">
										
									</div>
									<!-- Espaçamento entre menus -->
								</div>
								<!-- <p class="description">Anel em ouro amarelo.</p> -->
							</figcaption>			
						</span>
					</div>
				
					<div class="effect-men">

						<span class="cole">
							<li>
								<h4 class=""><?php echo $menuColecao; ?></h4>
							</li>
							<figcaption class="coleF col-md-12 pull-center">
								<div class="col-md-12">
									
								<!-- Colunas para espaçamento a esquerda do Figcaption do menu Categorias -->
									
								 <!-- Colunas para espaçamento a esquerda do Figcaption do menu Categorias -->
							
									<div class="col-md-2 col-sm-12 ativa" style="background-image: url(<?php echo $imagem; ?>web/images/Vicenza.png); background-position: bottom; background-size: cover; min-height: 150px;">
										<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuLima; ?>" class="text-wite">
											<h3><br><br><br></h3>
											<h4>
												<span><p class="menu-title"><?php echo $subMenuLima; ?></p></span>
											</h4>
										</a>

									</div>

									<div class="col-md-2 col-sm-12 ativa" style="background-image: url(<?php echo $imagem; ?>web/images/Selva.jpg); background-position: bottom; background-size: cover; min-height: 150px;">
										<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuSelva; ?>" class="text-wite">
											<h3><br><br><br></h3>
											<h4>
												<span><p class="menu-title"><?php echo $subMenuSelva; ?></p></span>
											</h4>
										</a>

									</div>
									<div class="col-md-2 col-sm-12 ativa" style="background-image: url(<?php echo $imagem; ?>web/images/Dunas.jpg); background-position: bottom; background-size: cover; min-height: 150px;">
										<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuDunas; ?>" class="text-wite">
											<h3><br><br><br></h3>
											<h4>
												<span><p class="menu-title"><?php echo $subMenuDunas; ?></p></span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-2 col-sm-12 ativa" style="background-image: url(<?php echo $imagem; ?>web/images/RJ.jpg); background-position: bottom; background-size: cover; min-height: 150px;">
										<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuRio; ?>" class="text-wite">
											<h3><br><br><br></h3>
											<h4>
												<span><p class="menu-title"><?php echo $subMenuRio; ?></p></span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-2 col-sm-12 ativa" style="background-image: url(<?php echo $imagem; ?>web/images/Fleur.jpg); background-position: bottom; background-size: cover; min-height: 150px;">
										<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuFleur; ?>" class="text-wite">
											<h3><br><br><br></h3>
											<h4>
												<span><p class="menu-title"><?php echo $subMenuFleur; ?></p></span>
											</h4>
										</a>
										
									</div>									
									<div class="col-md-2 col-sm-12 ativa" style="background-image: url(<?php echo $imagem; ?>web/images/Moscow.jpg); background-position: bottom; background-size: cover; min-height: 150px;">
										<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuMosc; ?>" class="text-wite">
											<h3><br><br><br></h3>
											<h4>
												<span><p class="menu-title"><?php echo $subMenuMosc; ?></p></span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-2 col-sm-12 ativa" style="background-image: url(<?php echo $imagem; ?>web/images/Singapura.jpg); background-position: bottom; background-size: cover; min-height: 150px;">
										<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuSing; ?>" class="text-wite">
											<h3><br><br><br></h3>
											<h4>
												<span><p class="menu-title"><?php echo $subMenuSing; ?></p></span>
											</h4>
										</a>
										
									</div>

								</div>
								<!-- <p class="description">Anel em ouro amarelo.</p> -->
							</figcaption>			
						</span>
					</div>
				
					<div class="effect-men">

						<span class="cata">
							<li>
								<h4 class=""><?php echo $menuCatalago; ?></h4>
							</li>
							<figcaption class="cataF col-md-12 pull-center">
								<div class="col-md-12">
								
									<!-- Div de espaçamento do menu Catálogo
									<div class="col-md-3">
										
									</div>
									 Div de espaçamento do menu Catálogo -->

									
										
											<div class="col-sm-2 col-md-2">
												<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuAneis; ?>" class="">
													<h5><img src="<?php echo $imagem; ?>web/images/icon/anel-geo.png" alt="" style="max-height: 70px; width: auto;"> </h5>
													<h4>
														<span><?php echo $subMenuAneis; ?></span>
													</h4>
												</a>
											
											</div>
											<div class="col-sm-2 col-md-2">
												<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuBrincos; ?>" class="">
													<h5><img src="<?php echo $imagem; ?>web/images/icon/brincos-geo.png" alt="" style="max-height: 70px; width: auto;"> </h5>
													<h4>
														<span><?php echo $subMenuBrincos; ?></span>
													</h4>
												</a>
												
											</div>
								
										
									
											<div class="col-sm-2 col-md-2">
												<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuConjuntos; ?>" class="">
													<h5><img src="<?php echo $imagem; ?>web/images/icon/conjunto-geo.png" alt="" style="max-height: 70px; width: auto;"> </h5>
													<h4>
														<span><?php echo $subMenuConjuntos; ?></span>
													</h4>
												</a>
											</div>
											<div class="col-sm-2 col-md-2">
												<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuGargantilhas; ?>" class="">
													<h5><img src="<?php echo $imagem; ?>web/images/icon/colar-geo.png" alt="" style="max-height: 70px; width: auto;"> </h5>
													<h4>
														<span><?php echo $subMenuGargantilhas; ?></span>
													</h4>
												</a>
												
											</div>
									
										
									
											<div class="col-sm-2 col-md-2">
												<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuPingentes; ?>" class="">
													<h5><img src="<?php echo $imagem; ?>web/images/icon/pingentes-geo.png" alt="" style="max-height: 70px; width: auto;"> </h5>
													<h4>
														<span><?php echo $subMenuPingentes; ?></span>
													</h4>
												</a>
												
											</div>
											<div class="col-sm-2 col-md-2">
												<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuPulseiras; ?>" class="">
													<h5><img src="<?php echo $imagem; ?>web/images/icon/pulseiras-geo.png" alt="" style="max-height: 70px; width: auto;"> </h5>
													<h4>
														<span><?php echo $subMenuPulseiras; ?></span>
													</h4>
												</a>
											</div>
									
							
								<!-- Div de espaçamento		
									
									</div>
									<div class="col-md-3"></div>

								 Div de espaçamento -->		
								
								
								<!-- <p class="description">Anel em ouro amarelo.</p> -->
							</figcaption>			
						</span>
					</div>

					<div class="effect-men">

						<span class="news">
							<li>
								<a href="<?php echo $idioma; ?>-<?php echo $lnkBlog; ?>"><h4 class=""><?php echo $menuBlog; ?></h4></a>
							</li>
							<!-- <figcaption class="newsF col-md-12">
								<div class="col-md-12">
									<div class="col-md-3">
										&nbps;
									</div>
									<div class="col-md-3">
										<a href="#" class="text-wite">
											<h3><span class="fa fa-google"></span></h3>
											<h4>
												<span> </span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-3">
										<a href="#" class="text-wite">
											<h3><span class="fa fa-envelope"></span></h3>
											<h4>
												<span> </span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-3">
										
									</div>
								</div>

							</figcaption> -->			
						</span>
						<span class="news">
							<li>
								<a href="<?php echo $idioma; ?>-<?php echo $lnkMenuContato; ?>"><h4 class=""><?php echo $subMenuContato; ?></h4></a>
							</li>
							<!-- <figcaption class="newsF col-md-12">
								<div class="col-md-12">
									<div class="col-md-3">
										&nbps;
									</div>
									<div class="col-md-3">
										<a href="#" class="text-wite">
											<h3><span class="fa fa-google"></span></h3>
											<h4>
												<span> </span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-3">
										<a href="#" class="text-wite">
											<h3><span class="fa fa-envelope"></span></h3>
											<h4>
												<span> </span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-3">
										
									</div>
								</div>

							</figcaption> -->			
						</span>
					</div>

					<div class="effect-men">

						<span class="idiom">
							<li>
								<h4 class=""><?php echo $menuIdioma; ?></h4>
							</li>
							<figcaption class="idiomF col-md-12">
								<div class="col-md-12">
									<div class="col-md-3">
										
									</div>
									<div class="col-md-2">
										<a href="pt<?php echo $link; ?>" class="">
											<h3><img src="<?php echo $imagem; ?>web/images/icon/pt.png" alt=""></h3>
											<h4>
												<span>Português</span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-2">
										<a href="en<?php echo $link; ?>" class="">
											<h3><img src="<?php echo $imagem; ?>web/images/icon/en.png" alt=""></h3>
											<h4>
												<span>English</span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-2">
										<a href="es<?php echo $link; ?>" class="">
											<h3><img src="<?php echo $imagem; ?>web/images/icon/es.png" alt=""></h3>
											<h4>
												<span>Español</span>
											</h4>
										</a>
										
									</div>
									<div class="col-md-3"> </div>
								</div>
								<!-- <p class="description">Anel em ouro amarelo.</p> -->
							</figcaption>			
						</span>
					</div>

				</nav>
			</div>
			</div>
		</nav>
			


			<nav class="navbar navbar-default">
  <div class="container-fluid">