<?php require('app/config/constants.php'); ?>
<?php require('app/control/Controller.php'); ?>



<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <title>GEO - Jóias 18K</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="description" content="<?php echo $homeDesc; ?>" />
  <meta name="keywords" content="<?php echo $lnkMenuAneis; ?>, <?php echo $lnkMenuBrincos; ?>, <?php echo $lnkMenuConjuntos; ?>, <?php echo $lnkMenuGargantilhas; ?>, <?php echo $lnkMenuPingentes; ?>, <?php echo $lnkMenuPulseiras; ?>, <?php echo $lnkLancamento; ?>" />  

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="estilo-sidebar.css">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

  <style>
    .wrapper { display: flex; width: 100%; align-items: stretch; }
    .wrapper { display: flex; align-items: stretch; }
    #sidebar { min-width: 250px; max-width: 250px; }
    #sidebar.active { margin-left: -250px; }
    #sidebar { min-width: 250px; max-width: 250px; min-height: 100vh; }

    a[data-toggle="collapse"] { position: relative; }
    .dropdown-toggle::after { display: block; position: absolute; top: 50%; right: 20px; transform: translateY(-50%); }

    @media (max-width: 768px) {
        #sidebar { margin-left: -250px; }
        #sidebar.active { margin-left: 0; }
    }
    
  </style>

</head>
<body>
  
  <div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">

        <div class="sidebar-header">
            <h3><img src="http://geo18k.com.br/web/images/LOGO-GEO.png" alt="Logo Geo18K"></h3>
        </div>

        <ul class="list-unstyled components">
            <section>
                <p>
                    <i class="fas fa-phone"></i><a href="tel:+55(17) 3233-8944"> +55(17) 3233-8944</a><br>
                    <i class="fas fa-mobile-alt"></i><a href="tel:+55(17) 99133-6990"> +55(17) 99133-6990</a><br>
                    <i class="far fa-envelope"></i><a href="mailto:atendimento@geo18k.com.br"> atendimento@geo18k.com.br</a>
                </p>
    
            </section>
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Coleções</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="https://www.google.com/"><img src="http://geo18k.com.br/web/images/Vicenza.png" style="width: 100%;"></a>
                    </li>
                    <li>
                        <a href="https://www.google.com/"><img src="http://geo18k.com.br/web/images/Selva.jpg" style="width: 100%;"></a>
                    </li>
                    <li>
                       <a href="https://www.google.com/"><img src="http://geo18k.com.br/web/images/Dunas.jpg" style="width: 100%;"></a>
                    </li>
                      <li>
                       <a href="https://www.google.com/"><img src="http://geo18k.com.br/web/images/RJ.jpg" style="width: 100%;"></a>
                    </li>
                       <li>
                       <a href="https://www.google.com/"><img src="http://geo18k.com.br/web/images/Fleur.jpg" style="width: 100%;"></a>
                    </li>
                      <li>
                       <a href="https://www.google.com/"><img src="http://geo18k.com.br/web/images/Moscow.jpg" style="width: 100%;"></a>
                    </li>
                    <li>
                       <a href="https://www.google.com/"><img src="http://geo18k.com.br/web/images/Singapura.jpg" style="width: 100%;"></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">Sobre a Geo</a>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Catálogo</a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li>
                        
                      <img class="float-left" src="http://geo18k.com.br/web/images/icon/anel-geo.png" alt="Anéis"><a href="#">Anéis</a><br>
                    </li>
                    <li>
                        
                      <img class="float-left" src="http://geo18k.com.br/web/images/icon/brincos-geo.png" alt="Anéis"><a href="#">Brincos</a><br>
                        
                    </li>
                    <li>
                        
                        <img class="float-left" src="http://geo18k.com.br/web/images/icon/conjunto-geo.png" alt="Anéis"><a href="#">Conjuntos</a><br>
                         
                    </li>
                    <li>
                        
                        <img class="float-left" src="http://geo18k.com.br/web/images/icon/colar-geo.png" alt="Anéis"><a href="#">Colares</a><br>
                         
                    </li>
                    <li>
                        
                        <img class="float-left" src="http://geo18k.com.br/web/images/icon/pingentes-geo.png" alt="Anéis"><a href="#">Pingentes</a><br>
                         
                    </li>
                    <li>
                         
                        <img class="float-left" src="http://geo18k.com.br/web/images/icon/pulseiras-geo.png" alt="Anéis"><a href="#">Pulseiras</a><br>
                         
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">Blog</a>
            </li>
            <li>
                <a href="#">Contato</a>
            </li>
            <li>
                 <a href="#idiomaSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Idioma</a>
                 <ul class="collapse list-unstyled" id="idiomaSubmenu">
                   <li>
                      <a href="#">Português</a>
                   </li>
                   <li>
                      <a href="#">English</a>
                   </li>
                   <li>
                     <a href="#">Espanol</a>
                   </li>
               </ul>
            </li>
        </ul>
    </nav>

<!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Toggle Sidebar</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Page</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Page</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Page</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Page</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            
            
            
        </div>
    </div>

</div>

  

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

</body>
</html>


  