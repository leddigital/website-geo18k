<?php



	/* PT */
	$idioma = "pt";
	$titulo = "GEO - Jóias 18k";
	$breve = "Em Breve!";

	/* MENU */
	$menuGeo 			= "A GEO";
		$subMenuSobre 	= "SOBRE A GEO";
		$subMenuContato = "CONTATO";

		$lnkMenuSobre 	= "Sobre-A-GEO";
		$lnkMenuContato = "Contato";

	$menuColecao = "COLEÇÕES";
		$subMenuSelva 	= "City Lights";
		$subMenuDunas	= "Dunas";
		$subMenuRio		= "Rio de Janeiro";
		$subMenuFleur	= "Fleur";
		$subMenuMosc	= "Moscow";
		$subMenuSing	= "Singapura";
		$subMenuLima	= "Vicenza";

		$lnkMenuSelva 	= "City-Lights";
		$lnkMenuDunas	= "Dunas";
		$lnkMenuRio		= "Rio-de-Janeiro";
		$lnkMenuFleur	= "Fleur";
		$lnkMenuMosc	= "Moscow";
		$lnkMenuSing	= "Singapura";
		$lnkMenuLima	= "Vicenza";

	$menuCatalago = "CATÁLOGO";
		$subMenuAneis 		 = "Anéis";
		$subMenuBrincos 	 = "Brincos";
		$subMenuConjuntos 	 = "Conjuntos";
		$subMenuGargantilhas = "Gargantilhas";
		$subMenuPingentes 	 = "Pingentes";
		$subMenuPulseiras 	 = "Pulseiras";

		$lnkMenuAneis 		 = "Aneis";
		$lnkMenuBrincos 	 = "Brincos";
		$lnkMenuConjuntos 	 = "Conjuntos";
		$lnkMenuGargantilhas = "Gargantilhas";
		$lnkMenuPingentes 	 = "Pingentes";
		$lnkMenuPulseiras 	 = "Pulseiras";

	$menuBlog = "BLOG";
	$lnkBlog  = "Blog";

	$menuIdioma = "IDIOMA";

	$menuLancamento = "Lançamentos";
	$lnkLancamento  = "Lancamentos";

	$homeDesc = "A GEO Joias é uma manufatura de joias imbuída em traduzir no design contemporâneo as suas joias que trazem conceitos e referências estéticos de diversos estilos de vida apresentados nas mais variadas culturas no globo terrestre.";

	$homeBlog = "Com a missão de adornar as pessoas com joias de ouro, manufaturadas por processos industriais e artesanais, que valorizem e enalteçam a essência cultural dos quatro cantos do mundo, a GEO Joias é uma manufatura de joias imbuída em traduzir no design contemporâneo as suas joias que trazem conceitos e referênciais estéticos de diversos estilos de vida apresentados nas mais variadas culturas do globo terrestre. <br><br> A GEO Joias possui certificação AMAGOLD, certificado nacional e internacional que garante a qualidade e o teor do ouro das peças produzidas.";

	$social = "Acompanhe nossas redes sociais";

	#ANEIS
	$AN1 =  "Anel em ouro amarelo com diamantes, citrino e ametista.";
	$AN2 =  "Anel em ouro amarelo com diamantes e ônix.";
	$AN3 =  "Anel em ouro amarelo, diamantes.";
	$AN4 =  "Anel em ouro branco e diamantes.";
	$AN5 =  "Anel em ouro amarelo, diamantes e ágata branca.";
	$AN6 =  "Anel em ouro amarelo, diamantes e ônix.";
	$AN7 =  "Anel em ouro amarelo.";
	$AN8 =  "Anel em ouro amarelo e diamantes.";
	$AN9 =  "Anel em ouro amarelo, diamantes e feldspato.";
	$AN10 =  "Anel em ouro amarelo, diamantes brancos e negros.";
	$AN11 =  "Anel EM OURO 18K.";
	$AN12 =  "Anel em ouro amarelo.";
	$AN13 =  "Anel em ouro amarelo e diamantes.";
	$AN14 =  "Anel em ouro amarelo e diamantes.";
	$AN15 =  "Anel em ouro amarelo e diamantes.";
	$AN16 =  "Anel em ouro amarelo, diamantes e feldspato marfim.";
	$AN17 =  "Anel em ouro amarelo e diamantes.";
	$AN18 =  "Anel em ouro amarelo e diamantes.";
	$AN19 =  "Anel em ouro amarelo e diamantes.";
	$AN20 =  "Anel em ouro amarelo, diamantes e nefrita.";
	$AN21 =  "Anel em ouro amarelo, diamantes e jade vermelha.";
	$AN22 =  "Anel em ouro amarelo, diamantes e citrino.";
	$AN23 =  "Anel em ouro amarelo e diamantes.";
	$AN24 =  "Anel em ouro amarelo e diamantes.";
	$AN25 =  "Anel em ouro amarelo e diamantes.";
	$AN26 =  "Anel em ouro amarelo, diamantes e ônix.";
	$AN27 =  "Anel de ouro 18k e diamantes.";
	$AN28 =  "Anel em ouro amarelo.";
	$AN29 =  "Anel EM OURO 18K.";
	$AN30 =  "Flyer - Anel em ouro amarelo e diamantes.";
	$AN31 =  "Chinatown - Anel em ouro amarelo e diamantes.";
	$AN32 =  "Flyer - Anel em ouro amarelo e diamantes.";
	$AN33 =  "Chinatown - Anel em ouro amarelo e diamantes.";
	$AN34 =  "Super Trees - Anel em ouro amarelo, diamantes e nefrita.";
	$AN35 =  "Anel de ouro 18k e diamantes.";
	$AN36 =  "Anel de ouro 18k e diamantes.";
	$AN37 =  "Anel de ouro 18k.";
	$AN38 =  "Anel de ouro 18k e diamantes.";
	$AN39 =  "Anel em diamantes em ouro amarelo 18k.";
	$AN40 =  "Anel em diamantes em ouro amarelo 18k.";
	$AN41 =  "Anel de ouro 18k e diamantes.";
	$AN42 =  "Anel de ouro 18k e diamantes.";
	$AN43 =  "Lenty - Anel em diamantes em ouro amarelo 18k.";
	$AN44 =  "Anel de ouro 18k e diamantes.";
	$AN45 =  "Catedral - Anel em diamantes em ouro amarelo 18k.";
	$AN46 =  "Faberge - Anel em diamantes em ouro amarelo 18k.";
	$AN47 =  "Moscovita - Anel em diamantes em ouro amarelo 18k.";
	$AN48 =  "Troika - Anel em diamantes em ouro amarelo 18k.";
	$AN49 =  "Bolshoi - Anel em diamantes em ouro amarelo 18k.";
	$AN50 =  "Czarina - Anel em diamantes em ouro amarelo 18k.";
	$AN51 =  "Anel em diamantes em ouro amarelo 18k.";
	$AN52 =  "Anel em ouro 18K com esmeralda e 10 diamantes de 0,5 pt.";
	$AN53 =  "Ipanema - Anel em ouro 18K com 15 diamantes de 1 pt.";
	$AN54 =  "Ipanema - Anel em ouro 18K com 15 diamantes de 1 pt.";
	$AN55 =  "Ipanema - Anel em ouro 18K com 14 diamantes de 1 pt e 1 diamante de 6 pts.";
	$AN56 =  "Copacabana - Anel em ouro 18K com 20 diamantes de 0,5 pt e 1 diamante de 6 pts.";
	$AN57 =  "Barra da Tijuca - Anel em ouro 18K com 20 diamantes de 0,5 pt e 2 diamante de 2,5 pts.";
	$AN58 =  "Leblon - Anel em ouro 18K com 21 diamantes de 1 pt.";
	$AN59 =  "Copacabana - Anel em ouro 18K com 9 diamantes de 0,5 pt.";
	$AN60 =  "Leblon - Anel em ouro 18K com 48 diamantes de 0,5 pt.";
	$AN61 =  "Barra da Tijuca - Anel em ouro 18K com 16 diamantes de 0,5 pt.";
	$AN62 =  "Anel EM OURO 18K.";
	$AN63 =  "Anel EM OURO 18K.";
	$AN64 =  "Anel em ouro 18k com 12 diamantes 1,0 mm.";
	$AN65 =  "Anel em ouro 18k com 14 diamantes de 1 mm.";
	$AN66 =  "Anel em ouro 18k com 97 diamantes de 1 mm.";
	$AN67 =  "Anel em ouro 18k com 7 diamantes de  1mm.";
	$AN68 =  "Anel em ouro 18k com 7 diamantes 1,0 mm.";
	$AN69 =  "Anel em ouro 18k  com 16 diamantes de 1 mm.";
	$AN70 =  "Anel EM OURO 18K.";
	$AN71 =  "Anel EM OURO 18K.";
	$AN72 =  "Anel em ouro 18k com 22 diamantes de 1 mm e gema redonda.";
	$AN73 =  "Anel em ouro amarelo e diamantes.";
	$AN74 =  "Samovar - Anel em ouro amarelo e diamantes.";
	$AN75 =  "Anel em ouro 18k com 42 diamantes de (0,5pt).";
	$AN76 =  "Anel em ouro 18k com 48 diamantes de (0,5 pt).";
	$AN77 =  "Anel em ouro 18k com 21 diamantes de (0,5 pt).";
	$AN78 =  "Anel em ouro 18k com 11 diamantes de 1 pt.";
	$AN79 =  "Anel em ouro 18k com 32 diamantes de 0,5 pt.";
	$AN80 =  "Anel em ouro 18k com 4 diamantes de 0,5 pt.";
	$AN81 =  "Anel em ouro 18k com 18 diamantes de 0,5 pt.";
	$AN82 =  "Anel em ouro 18k com 34 diamantes de 0,5 pt.";
	$AN83 =  "Anel em ouro18k com 21 diamantes de 0,5 pt.";
	$AN84 =  "Anel em ouro 18k com 24 diamantes de 0,5 pt.";
	$AN85 =  "Anel em ouro 18k com 18 diamantes de 0,5 pt.";
	$AN86 =  "Anel em ouro 18k com 19 diamantes de 0,5 pt.";

	
	// CÓDIGO ANEIS
	$ANCOD = array(
		"AN27", "AN276", "AN291", "AN311", "AN327", "AN351A", "AN355", "AN355A", "AN363", "AN378", "AN378B", "AN379", "AN415", "AN437", "AN443", "AN445B", "AN465", "AN487", "AN540M", "AN562", "AN576", "AN592B", "AN607", 
		"AN620", "AN623B", "AN720", "AN720A", "AN735", "AN735B", "AN757", "AN771", "AN774", "AN778", "AN780", "AN794", "AN796", "AN797", "AN798A", "AN798B", "AN798C", "AN799", "AN800", "AN801", "AN799", "AN803", "AN804", 
		"AN805", "AN807", "AN808", "AN811", "AN816", "AN827", "AN832", "AN834", "AN835","AN836", "AN837", "AN841", "AN842", "AN850", "AN857", "AN859", "AN860", "AN868", "AN869", "AN870", "AN871", "AN873", "AN875", "AN877",
		"AN878", "AN880", "NA725", "AN802", "AN765A", "AN919", "AN899", "AN906", "AN908", "AN911", "AN912", "AN913", "AN915", "AN916", "AN917", "AN918"
	);

	// DESCRIÇÃO ANÉIS EM PT-BR
	$ANPT = array(
		"Anel em ouro amarelo com diamantes, citrino e ametista.",
		"Anel em ouro amarelo com diamantes e ônix.",
		"Anel em ouro amarelo, diamantes.",
		"Anel em ouro branco e diamantes.",
		"Anel em ouro amarelo, diamantes e ágata branca.",
		"Anel em ouro amarelo, diamantes e ônix.",
		"Anel em ouro amarelo.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo, diamantes e feldspato.",
		"Anel em ouro amarelo, diamantes brancos e negros.",
		"Anel EM OURO 18K.",
		"Anel em ouro amarelo.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo, diamantes e feldspato marfim.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo, diamantes e nefrita.",
		"Anel em ouro amarelo, diamantes e jade vermelha.",
		"Anel em ouro amarelo, diamantes e citrino.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo e diamantes.",
		"Anel em ouro amarelo, diamantes e ônix.",
		"Anel de ouro 18k e diamantes.",
		"Anel em ouro amarelo.",
		"Anel EM OURO 18K.",
		"Flyer - Anel em ouro amarelo e diamantes.",
		"Chinatown - Anel em ouro amarelo e diamantes.",
		"Flyer - Anel em ouro amarelo e diamantes.",
		"Chinatown - Anel em ouro amarelo e diamantes.",
		"Super Trees - Anel em ouro amarelo, diamantes e nefrita.",
		"Anel de ouro 18k e diamantes.",
		"Anel de ouro 18k e diamantes.",
		"Anel de ouro 18k.",
		"Anel de ouro 18k e diamantes.",
		"Anel em diamantes em ouro amarelo 18k.",
		"Anel em diamantes em ouro amarelo 18k.",
		"Anel de ouro 18k e diamantes.",
		"Anel de ouro 18k e diamantes.",
		"Lenty - Anel em diamantes em ouro amarelo 18k.",
		"Anel de ouro 18k e diamantes.",
		"Catedral - Anel em diamantes em ouro amarelo 18k.",
		"Faberge - Anel em diamantes em ouro amarelo 18k.",
		"Moscovita - Anel em diamantes em ouro amarelo 18k.",
		"Troika - Anel em diamantes em ouro amarelo 18k.",
		"Bolshoi - Anel em diamantes em ouro amarelo 18k.",
		"Czarina - Anel em diamantes em ouro amarelo 18k.",
		"Anel em diamantes em ouro amarelo 18k.",
		"Anel em ouro 18K com esmeralda e 10 diamantes de 0,5 pt.",
		"Ipanema - Anel em ouro 18K com 15 diamantes de 1 pt.",
		"Ipanema - Anel em ouro 18K com 15 diamantes de 1 pt.",
		"Ipanema - Anel em ouro 18K com 14 diamantes de 1 pt e 1 diamante de 6 pts.",
		"Copacabana - Anel em ouro 18K com 20 diamantes de 0,5 pt e 1 diamante de 6 pts.",
		"Barra da Tijuca - Anel em ouro 18K com 20 diamantes de 0,5 pt e 2 diamante de 2,5 pts.",
		"Leblon - Anel em ouro 18K com 21 diamantes de 1 pt.",
		"Copacabana - Anel em ouro 18K com 9 diamantes de 0,5 pt.",
		"Leblon - Anel em ouro 18K com 48 diamantes de 0,5 pt.",
		"Barra da Tijuca - Anel em ouro 18K com 16 diamantes de 0,5 pt.",
		"Anel EM OURO 18K.",
		"Anel EM OURO 18K.",
		"Anel em ouro 18k com 12 diamantes 1,0 mm.",
		"Anel em ouro 18k com 14 diamantes de 1 mm.",
		"Anel em ouro 18k com 97 diamantes de 1 mm.",
		"Anel em ouro 18k com 7 diamantes de  1mm.",
		"Anel em ouro 18k com 7 diamantes 1,0 mm.",
		"Anel em ouro 18k  com 16 diamantes de 1 mm.",
		"Anel EM OURO 18K.",
		"Anel EM OURO 18K.",
		"Anel em ouro 18k com 22 diamantes de 1 mm e gema redonda.",
		"Anel em ouro amarelo e diamantes.",
		"Samovar - Anel em ouro amarelo e diamantes.",
		"Anel em ouro 18k com 42 diamantes de (0,5pt).",
		"Anel em ouro 18k com 48 diamantes de (0,5 pt).",
		"Anel em ouro 18k com 21 diamantes de (0,5 pt).",
		"Anel em ouro 18k com 11 diamantes de 1 pt.",
		"Anel em ouro 18k com 32 diamantes de 0,5 pt.",
		"Anel em ouro 18k com 4 diamantes de 0,5 pt.",
		"Anel em ouro 18k com 18 diamantes de 0,5 pt.",
		"Anel em ouro 18k com 34 diamantes de 0,5 pt.",
		"Anel em ouro18k com 21 diamantes de 0,5 pt.",
		"Anel em ouro 18k com 24 diamantes de 0,5 pt.",
		"Anel em ouro 18k com 18 diamantes de 0,5 pt.",
		"Anel em ouro 18k com 19 diamantes de 0,5 pt.",
	);
		

	// Upload de novos produtos dia 01/02/2019
	$AN8567  =  "Anel em ouro amarelo com diamantes";
	$AN8612  =  "Anel em ouro amarelo com diamantes";
	$AN8726  =  "Anel em ouro amarelo com diamantes";
	$AN525B  =  "Anel em ouro amarelo com esmeralda ";
	$AN600G  =  "Anel em ouro amarelo com Nefrita ";
	$AN621BA  =  "Anel em ouro amarelo com esmeralda ";
	$AN621BD  =  "Anel em ouro amarelo com esmeralda ";
	$AN920  =  "Anel em ouro amarelo com diamantes";
	$AN928  =  "Anel em ouro amarelo com Safira ";
	$AN930  =  "Anel em ouro amarelo com diamantes";
	$AN932B  =  "Anel em ouro amarelo com diamantes";
	$AN933  =  "Anel em ouro amarelo com diamantes";
	$AN934  =  "Anel em ouro amarelo com diamantes";
	$AN935  = "Anel em ouro amarelo com diamantes";




	#BRINCOS
	$BR241 = "Brinco em ouro amarelo e diamantes.";
	$BR346P = "Brinco em ouro amarelo e diamantes.";
	$BR352 = "Brinco em ouro amarelo e diamantes e ágata branca.";
	$BR362C = "Brinco em ouro amarelo e diamantes.";
	$BR363 = "Brinco de ouro 18k e diamantes.";
	$BR366 = "Brinco em ouro 18k com 12 diamantes de 0,5 pt.";
	$BR558 = "Brinco em ouro amarelo, diamantes e nefrita.";
	$BR581 = "Brinco de ouro 18k, diamantes e ágata branca.";
	$BR600C = "Brinco em ouro amarelo e diamantes fumê.";
	$BR603 = "Brinco em ouro amarelo e diamantes.";
	$BR606 = "Brinco em ouro amarelo e diamantes.";
	$BR619 = "Brinco em ouro amarelo e diamantes.";
	$BR755 = "Super Trees - Brinco em ouro amarelo e diamantes.";
	$BR756 = "Litle India - Brinco em ouro amarelo, diamantes e nefrita.";
	$BR757A = "Flyer - Brinco em ouro amarelo e diamante.";
	$BR757B = "Flyer - Brinco em ouro amarelo e diamantes.";
	$BR767 = "Brinco em ouro amarelo e diamantes.";
	$BR771 = "Litle India - Brinco em ouro amarelo e diamantes.";
	$BR772 = "Litle India - Brinco em ouro amarelo e diamantes.";
	$BR774 = "Brinco em ouro amarelo e diamantes.";
	$BR775 = "Brinco em ouro amarelo e diamantes.";
	$BR776 = "Litle India - Brinco em ouro amarelo e diamantes.";
	$BR780 = "Super Trees - Brinco em ouro amarelo, diamantes e nefrita.";
	$BR793 = "Brinco em ouro 18k com 26 diamantes de 1pt.";
	$BR793B = "Brinco em ouro 18K com 24 bris de 1 pt.";
	$BR794 = "Brinco de ouro 18k e diamantes.";
	$BR798B = "Brinco de ouro 18k e diamantes.";
	$BR798A = "Brinco de ouro 18k e diamantes.";
	$BR798C = "Brinco de ouro 18k e diamantes.";
	$BR799 = "Brinco em ouro 18k com 16 diamantes de 1 pt e 28 diamantes de 0,5 pt.";
	$BR800 = "Brinco de 18k e diamantes.";
	$BR801 = "Lenty - Brinco de ouro 18k e diamantes.";
	$BR803 = "Catedral - Brinco de ouro 18k e diamantes.";
	$BR804 = "Faberge - Brinco de ouro 18k e diamantes.";
	$BR805B = "Moscovita - Brinco de ouro 18k e diamantes.";
	$BR806 = "Bolshoi - Brinco de ouro 18k e diamantes.";
	$BR807 = "Troika - Brinco de ouro 18k e diamantes.";
	$BR811 = "Czarina - Brinco de ouro 18k e diamantes.";
	$BR814A = "Kandinsky - Brinco de ouro 18k e diamantes.";
	$BR822 = "Brinco em ouro 18k.";
	$BR826 = "Brinco em ouro 18k com esmeraldas e 28 diamantes de 0,5 pt.";
	$BR827 = "Brinco em ouro 18k com 20 diamantes 0,5 pt e safiras.";
	$BR869 = "Brinco em ouro 18k com 26 diamantes de 1 mm.";
	$BR872 = "Brinco em ouro 18k com 14 diamantes de 1 mm.";
	$BR873 = "Brinco em ouro 18k com 40 diamantes de 1 mm e 2 diamantes de 1,30 mm.";
	$BR875 = "Brinco em ouro 18k com 36 diamantes de 1 mm.";
	$BR880 = "Brinco em ouro 18k com 54 diamantes de 1 mm e gema redonda.";
	$BR805 = "Moscovita - Brincos em ouro 18k e diamantes.";
	$BR899 = "Vicenza - Brincos em ouro 18k com 22 diamantes de 0,5 pt.";
	$BR907 = "Vicenza - Brincos em ouro 18k com 16 diamantes de 0,5 pt.";
	$BR911 = "Vicenza - Brincos em ouro 18k com 8 diamantes de 0,5 pt.";
	$BR918 = "Vicenza - Brincos em ouro 18k com 26 diamantes de 0,5 pt.";

	#CONJUNTOS
	$C562 = "Conjunto de brinco e colar em nefrita e diamantes em ouro amarelo 18k.";
	$C720 = "Conjunto de brinco e Anel em ônix e diamantes em ouro amarelo 18k.";
	$C757A = "Conjunto de brinco e Anel em diamantes em ouro amarelo 18k.";
	$C798C = "Conjunto de brinco e Anel em diamantes em ouro amarelo 18k.";
	$C800 = "Conjunto de brinco e Anel em diamantes em ouro amarelo 18k.";
	$Conjunto911 = "Conjunto de brinco e Anel com 12 diamantes em ouro amarelo.";
	$Conjunto918 = "Conjunto de brinco e Anel com 45 diamantes em ouro amarelo.";
	$Conjunto899 = "Conjunto de brinco e Anel com 43 diamantes em ouro amarelo.";

	#PINGENTES
	$PI128 = "Pingente em ouro 18k.";
	$PI429 = "Pingente em ouro amarelo e diamantes.";
	$PI464 = "Pingente em ouro amarelo.";
	$PI562 = "Pingente em ouro amarelo e diamantes, nefrita.";
	$PI596 = "Pingente em ouro amarelo e diamantes.";
	$PI771 = "Litle India - Pingente em ouro amarelo e diamantes.";
	$PI776 = "Litle India - Pingente em ouro amarelo e diamantes.";
	$PI798B = "Pingente em ouro amarelo e diamantes.";
	$PI832 = "Pingente em ouro 18k com 34 diamantes de 1,5 pt e 7 diamantes de 0,5 pt.";
	$PI833 = "Pingente em ouro 18k com 27 diamantes de 0,5pt.";
	$PI837 = "Pingente em ouro 18k com 24 diamantes de 0,5 pt.";
	$PI838 = "Guanabara - Pingente em ouro 18k com 16 diamantes de 0,5 pt.";
	$PI869 = "Pingente em ouro 18k com 22 diamantes de 1 mm.";
	$PI871 = "Pingente em ouro 18k com 7 diamantes de 1 mm.";

	#PULSEIRAS
	$PUL81 = "Pulseira com 18 diamantes de 0,5 pt.";
	$PUL19 = "Pulseira em ouro amarelo e diamantes.";
	$PUL17 = "Pulseira em ouro 18k com 28 diamantes de 0,5 pt.";
	$PUL494 = "Pulseira em ouro 18k com 18 diamantes de 0,5 pt.";
	$PUL798CA = "Pulseira em ouro 18k com 24 diamantes de 0,5 pt.";



if (preg_match("/en/", $url)){

	if ((preg_match("/pt-Pingentes/", $url)) or (preg_match("/pt-Lancamentos/", $url)) or (preg_match("/pt-Vicenza/", $url))){

	}else{

		if ((preg_match("/es-Pingentes/", $url)) or (preg_match("/es-Lancamentos/", $url)) or (preg_match("/es-Vicenza/", $url))){

		}else{


	/* EN */
	$idioma = "en";
	$titulo = "GEO - Jewels 18k";
	$breve = "Coming soon!";
	
	/* MENU */
	$menuGeo 			= "THE GEO";
		$subMenuSobre 	= "ABOUT GEO";
		$subMenuContato = "CONTACT";

		$lnkMenuSobre 	= "About-GEO";
		$lnkMenuContato = "Contact";

	$menuColecao = "COLLECTIONS";
		$subMenuSelva 	= "City Lights";
		$subMenuDunas	= "Dunes";
		$subMenuRio		= "Rio de Janeiro";
		$subMenuFleur	= "Fleur";
		$subMenuMosc	= "Moscow";
		$subMenuSing	= "Singapore";
		$subMenuLima	= "Vicenza";

		$lnkMenuSelva 	= "City-Lights";
		$lnkMenuDunas	= "Dunes";
		$lnkMenuRio		= "Rio-de-Janeiro";
		$lnkMenuFleur	= "Fleur";
		$lnkMenuMosc	= "Moscow";
		$lnkMenuSing	= "Singapore";
		$lnkMenuLima	= "Vicenza";

	$menuCatalago = "CATALOG";
		$subMenuAneis 		 = "Rings";
		$subMenuBrincos 	 = "Earrings";
		$subMenuConjuntos 	 = "Sets";
		$subMenuGargantilhas = "Chokers";
		$subMenuPingentes 	 = "Pendants";
		$subMenuPulseiras 	 = "Bracelets";

		$lnkMenuAneis 		 = "Rings";
		$lnkMenuBrincos 	 = "Earrings";
		$lnkMenuConjuntos 	 = "Sets";
		$lnkMenuGargantilhas = "Chokers";
		$lnkMenuPingentes 	 = "Pendants";
		$lnkMenuPulseiras 	 = "Bracelets";

	$menuBlog = "BLOG";
	$lnkBlog  = "Blog";

	$menuIdioma = "LANGUAGE";

	$menuLancamento = "New Arrivals";
	$lnkLancamento  = "New-Arrivals";

	$homeDesc = "The Geo Joias is certified Amagold, national and international certificate that guarantees the quality and gold content of the pieces produced.";

	$homeBlog = "With the mission to bring to people gold jewelry, produced both by industrial processesand by handcrafted processes, that values and enhances the different cultural essences ofthe world, the GEO Jewelry is a jewelry manufacturer that aims to translate its jewels intoa contemporary design, since they present concepts and aesthetic references fromdifferent lifestyles belonging to the most varied cultures in the world. <br><br> The Geo Joias is certified Amagold, national and international certificate that guaranteesthe quality and gold content of the pieces produced.";

	$social = "Follow our social networks";


$AN273 = "Yellow gold ring with diamonds, citrine and amethyst.";
$AN276 = "Yellow gold ring with diamonds and onyx.";
$AN291 = "Ring in yellow gold, diamonds.";
$AN311 = "Ring in white gold and diamonds.";
$AN327 = "Ring in yellow gold, diamonds and white agate.";
$AN351A = "Ring in yellow gold, diamonds and onyx.";
$AN355 = "Ring in yellow gold.";
$AN355A = "Ring in yellow gold and diamonds.";
$AN363 = "Ring in yellow gold, diamonds and feldspar.";
$AN378 = "Ring in yellow gold, white and black diamonds.";
$AN378B = "18K GOLD RING.";
$AN379 = "Ring in yellow gold.";
$AN415 = "Ring in yellow gold and diamonds.";
$AN437 = "Ring in yellow gold and diamonds.";
$AN443 = "Ring in yellow gold and diamonds.";
$AN445B = "Ring in yellow gold, diamonds and ivory feldspar.";
$AN465 = "Ring in yellow gold and diamonds.";
$AN487 = "Ring in yellow gold and diamonds.";
$AN540M = "Ring in yellow gold and diamonds.";
$AN562 = "Ring in yellow gold, diamonds and nephrite.";
$AN576 = "Ring in yellow gold, diamonds and red jade.";
$AN592B = "Ring in yellow gold, diamonds and citrus.";
$AN607 = "Ring in yellow gold and diamonds.";
$AN620 = "Ring in yellow gold and diamonds.";
$AN623B = "Ring in yellow gold and diamonds.";
$AN720 = "Ring in yellow gold, diamonds and onyx.";
$AN720A = "18k gold ring and diamonds.";
$AN735 = "Ring in yellow gold.";
$AN735B = "18K GOLD RING.";
$AN757 = "Flyer - Ring in yellow gold and diamonds.";
$AN771 = "Chinatown - Ring in yellow gold and diamonds.";
$AN774 = "Flyer - Ring in yellow gold and diamonds.";
$AN778 = "Chinatown - Ring in yellow gold and diamonds.";
$AN780 = "Super Trees - Yellow gold, diamond and nephrite ring.";
$AN794 = "18k gold ring and diamonds.";
$AN796 = "18k gold ring and diamonds.";
$AN797 = "18k gold ring.";
$AN798A = "18k gold ring and diamonds.";
$AN798B = "Ring in 18k yellow gold diamonds";
$AN798C = "Ring in 18k yellow gold diamonds.";
$AN799 = "18k gold ring and diamonds.";
$AN800 = "18k gold ring and diamonds.";
$AN801 = "Lenty - Ring in 18k yellow gold diamonds";
$AN799 = "18k gold ring and diamonds.";
$AN803 = "Catedral - Ring in 18k yellow gold diamonds.";
$AN804 = "Faberge - Ring in 18k yellow gold diamonds.";
$AN805 = "Moscovita - Ring in 18k yellow gold diamonds.";
$AN807 = "Troika - Ring in 18k yellow gold diamonds.";
$AN808 = "Bolshoi - Ring in 18k yellow gold diamonds.";
$AN811 = "Czarina - Ring in 18k yellow gold diamonds.";
$AN816 = "Ring in 18k yellow gold diamonds";
$AN827 = "18k gold ring with emerald and 10 diamonds of 0.5 pt.";
$AN832 = "Ipanema - 18K gold ring with 15 1-pt diamonds";
$AN834 = "Ipanema - 18K gold ring with 15 1-pt diamonds";
$AN835 = "Ipanema - 18K gold ring with 14 1-pt. Diamonds and 1 6-ply diamond.";
$AN836 = "Copacabana - 18K gold ring with 20 diamonds of 0.5 pt and 1 diamond of 6 pts.";
$AN837 = "Barra da Tijuca - 18K gold ring with 20 diamonds of 0.5 pt and 2 diamond of 2,5 pts.";
$AN841 = "Leblon - 18K gold ring with 21 1-pt diamonds";
$AN842 = "Copacabana - 18K gold ring with 9 diamonds of 0.5 pt.";
$AN850 = "Leblon - 18K gold ring with 48 diamonds of 0.5 pt.";
$AN857 = "Barra da Tijuca - 18K gold ring with 16 diamonds of 0.5 pt.";
$AN859 = "18K GOLD RING.";
$AN860 = "18K GOLD RING.";
$AN868 = "18k gold ring with 12 diamonds 1.0mm.";
$AN869 = "18k gold ring with 14 1mm diamonds";
$AN870 = "18k gold ring with 97 1mm diamonds";
$AN871 = "18k gold ring with 7 1mm diamonds.";
$AN873 = "18k gold ring with 7 diamonds 1.0mm.";
$AN875 = "18k gold ring with 16 1mm diamonds";
$AN877 = "18K GOLD RING.";
$AN878 = "18K GOLD RING.";
$AN880 = "18k gold ring with 22 1mm diamonds and round gem.";
$NA725 = "Ring in yellow gold and diamonds.";
$AN802 = "Samovar - Ring in yellow gold and diamonds.";
$AN765A= "Ring in white gold with 42 diamonds (0,5 pt).";
$AN919 = "Ring in yellow gold with 48 diamonds (0,5 pt).";
$AN899 = "Ring in yellow gold with 21 diamonds (0,5 pt).";
$AN906 = "Ring in yellow gold with 11 diamonds (0,5 pt).";
$AN908 = "Ring in yellow gold with 32 diamonds (0,5 pt).";
$AN911 = "Ring in yellow gold with 4 diamonds (0,5 pt).";
$AN912 = "Ring in yellow gold with 18 diamonds (0,5 pt).";
$AN913 = "Ring in yellow gold with 34 diamonds (0,5 pt).";
$AN915 = "Ring in yellow gold with 21 diamonds (0,5 pt).";
$AN916 = "Ring in yellow gold with 24 diamonds (0,5 pt).";
$AN917 = "Ring in yellow gold wit 18 diamonds (0,5 pt).";
$AN918 = "Ring in yellow gold with 19 diamonds (0,5 pt).";

$BR241 = "Earring in yellow gold and diamonds.";
$BR346P = "Earring in yellow gold and diamonds.";
$BR352 = "Earring in yellow gold and diamonds and white agate.";
$BR362C = "Earring in yellow gold and diamonds.";
$BR363 = "18k gold earring and diamonds.";
$BR366 = "Earring in 18k gold with 12 diamonds of 0.5 pt.";
$BR558 = "Earring in yellow gold, diamonds and nephrite.";
$BR581 = "18k gold earring, diamonds and white agate.";
$BR600C = "Earring in yellow gold and smoky diamonds.";
$BR603 = "Earring in yellow gold and diamonds.";
$BR606 = "Earring in yellow gold and diamonds.";
$BR619 = "Earring in yellow gold and diamonds.";
$BR755 = "Super Trees - Earring in yellow gold and diamonds.";
$BR756 = "Litle India - Earring in yellow gold, diamonds and nephrite.";
$BR757A = "Flyer - Earring in yellow gold and diamond.";
$BR757B = "Earring in yellow gold and diamonds.";
$BR767 = "Earring in yellow gold and diamonds.";
$BR771 = "Litle India - Earring in yellow gold and diamonds.";
$BR772 = "Litle India - Earring in yellow gold and diamonds.";
$BR774 = "Earring in yellow gold and diamonds.";
$BR775 = "Earring in yellow gold and diamonds.";
$BR776 = "Litle India - Earring in yellow gold and diamonds.";
$BR780 = "Super Trees - Earring in yellow gold, diamonds and nephrite.";
$BR793 = "18k gold earring with 26 1pt diamonds";
$BR793B = "Earring in 18K gold with 24 bris of 1 pt.";
$BR794 = "18k gold earring and diamonds.";
$BR798B = "18k gold earring and diamonds.";
$BR798A = "18k gold earring and diamonds.";
$BR798C = "18k gold earring and diamonds.";
$BR799 = "18k gold earring with 16 1-pt diamonds and 28 0.5-pt diamonds";
$BR800 = "18k diamond earring.";
$BR801 = "Lenty - 18k gold earring and diamonds.";
$BR803 = "Catedral - 18k gold earring and diamonds.";
$BR804 = "Faberge - 18k gold earring and diamonds.";
$BR805B = "Moscovita - 18k gold earring and diamonds.";
$BR806 = "Bolshoi - 18k gold earring and diamonds.";
$BR807 = "Troika - 18k gold earring and diamonds.";
$BR811 = "Czarina - 18k gold earring and diamonds.";
$BR814A = "Kandinsky - 18k gold earring and diamonds.";
$BR822 = "Earring in 18k gold.";
$BR826 = "Earring in 18k gold with emeralds and 28 diamonds of 0.5 pt.";
$BR827 = "Earring in 18k gold with 20 diamonds of 0.5 pt and sapphires.";
$BR869 = "Earring in 18k gold with 26 1mm diamonds.";
$BR872 = "Earring in 18k gold with 14 1mm diamonds.";
$BR873 = "Earring in 18k gold with 40 1mm diamonds and 2 diamonds of 1.30mm.";
$BR875 = "Earring in 18k gold with 36 1mm diamonds.";
$BR880 = "Earring in 18k gold with 54 1mm diamonds and round gem.";
$BR805 = "Moscovita - 18k gold earring and diamonds.";
$BR899 = "Vicenza - Earring in yellow gold with 22 diamonds (0,5 pt).";
$BR907 = "Vicenza - Earring in yellow gold with 16 diamonds (0,5 pt).";
$BR911 = "Vicenza - Earring in yellow gold with 8 diamonds (0,5 pt).";
$BR918 = "Vicenza - Earring in yellow gold with 26 diamonds (0,5 pt).";


$C562 = "Set of earrings and necklace in nephrite and diamonds in 18k yellow gold.";
$C720 = "Onyx and diamond ring set in 18k yellow gold.";
$C757A = "18k Yellow Gold Diamond Earring and Ring Set";
$C798C = "Earring and ring set in 18k yellow gold diamonds.";
$C800 = "18k Yellow Gold Diamond Earring and Ring Set";
$Conjunto911 = "Earring and ring set made of 12 diamonds in yellow gold.";
$Conjunto918 = "Earring and ring set made of 45 diamonds in yellow gold.";
$Conjunto899 = "Earring and ring set made of 43 diamonds in yellow gold.";

$PI128 = "Pendant in 18k gold.";
$PI429 = "Yellow gold and diamond pendant.";
$PI464 = "Yellow gold pendant.";
$PI562 = "Pendant in yellow gold and diamonds, nephrite.";
$PI596 = "Yellow gold and diamond pendant.";
$PI771 = "Litle India - Yellow gold and diamond pendant.";
$PI776 = "Litle India - Yellow gold and diamond pendant.";
$PI798B = "Yellow gold and diamond pendant.";
$PI832 = "Pendant in 18k gold with 34 diamonds of 1,5 pt and 7 diamonds of 0,5 pt.";
$PI833 = "Pendant in 18k gold with 27 diamonds of 0.5pt.";
$PI837 = "Pendant in 18k gold with 24 diamonds of 0.5 pt.";
$PI838 = "Guanabara - Pendant in 18k gold with 16 diamonds of 0.5 pt.";
$PI869 = "Pendant in 18k gold with 22 1mm diamonds.";
$PI871 = "Pendant in 18k gold with 7 1mm diamonds.";

$PUL81 = "Bracelet with 18 diamonds of 0.5 pt.";
$PUL19 = "Bracelet in yellow gold and diamonds.";
$PUL17 = "Bracelet in yellow gold with 28 diamonds (0,5 pt).";
$PUL494 = "Bracelet in yellow gold with 18 diamonds (0,5 pt).";
$PUL798CA = "Bracelet in yellow gold with 24 diamonds (0,5 pt).";

		}

	}

}

if ((preg_match("/es/", $url)) or (preg_match("/es-Pingentes/", $url)) or (preg_match("/es-Vicenza/", $url))){

	/* ES */
	$idioma = "es";
	$titulo = "GEO - Joyas 18k";
	$breve = "¡En breve!";

	/* MENU */
	$menuGeo 			= "LA GEO";
		$subMenuSobre 	= "SOBRE LA GEO";
		$subMenuContato = "CONTACTO";

		$lnkMenuSobre 	= "Sobre-La-GEO";
		$lnkMenuContato = "Contacto";

	$menuColecao = "COLECCIONES";
		$subMenuSelva 	= "City Lights";
		$subMenuDunas	= "Dunas";
		$subMenuRio		= "Rio de Janeiro";
		$subMenuFleur	= "Fleur";
		$subMenuMosc	= "Moscú";
		$subMenuSing	= "Singapur";
		$subMenuLima	= "Vicenza";

		$lnkMenuSelva 	= "City-Lights";
		$lnkMenuDunas	= "Dunas";
		$lnkMenuRio		= "Rio-de-Janeiro";
		$lnkMenuFleur	= "Fleur";
		$lnkMenuMosc	= "Moscu";
		$lnkMenuSing	= "Singapur";
		$lnkMenuLima	= "Vicenza";

	$menuCatalago = "CATÁLOGO";
		$subMenuAneis 		 = "Anillos";
		$subMenuBrincos 	 = "Pendientes";
		$subMenuConjuntos 	 = "Conjuntos";
		$subMenuGargantilhas = "Gargantillas";
		$subMenuPingentes 	 = "Colgantes";
		$subMenuPulseiras 	 = "Pulseras";

		$lnkMenuAneis 		 = "Anillos";
		$lnkMenuBrincos 	 = "Pendientes";
		$lnkMenuConjuntos 	 = "Conjuntos";
		$lnkMenuGargantilhas = "Gargantillas";
		$lnkMenuPingentes 	 = "Colgantes";
		$lnkMenuPulseiras 	 = "Pulseras";

	$menuBlog = "BLOG";
	$lnkBlog  = "Blog";

	$menuIdioma = "IDIOMA";

	$menuLancamento = "Novedades";
	$lnkLancamento  = "Novedades";

	$homeDesc = "La Geo Joyas tiene certificación Amagold, certificado nacional e internacional que garantiza la calidad y el contenido del oro de las piezas producidas.";

	$homeBlog = "Con la misión de llevar a la gente joyas de oro, producidas tanto por procesos industriales como por procesos artesanales, que valoren y realcen las diferentes esencias culturales del mundo, la GEO Joyas es un fabricante de joyas que tiene como objetivo traducir sus joyas en un diseño contemporáneo, ya que presentan conceptos y referencias estéticas de diferentes estilos de vida pertenecientes a las más diversas culturas del mundo. <br><br> La Geo Joyas tiene certificación Amagold, certificado nacional e internacional que garantiza la calidad y el contenido del oro de las piezas producidas.";


	$social = "Acompañe nuestras redes sociales";

$AN273 = "Anillo en oro amarillo con diamantes, cítricos y amatistas.";
$AN276 = "Anillo en oro amarillo con diamantes y ónice.";
$AN291 = "Anillo en oro amarillo, diamantes.";
$AN311 = "Anillo en oro blanco y diamantes.";
$AN327 = "Anillo en oro amarillo, diamantes y ágata blanca.";
$AN351A = "Anillo en oro amarillo, diamantes y ónice.";
$AN355 = "Anillo en oro amarillo.";
$AN355A = "Anillo en oro amarillo y diamantes.";
$AN363 = "Anillo en oro amarillo, diamantes y feldspato.";
$AN378 = "Anillo en oro amarillo, diamantes blancos y negros.";
$AN378B = "Anillo EN ORO 18K.";
$AN379 = "Anillo en oro amarillo.";
$AN415 = "Anillo en oro amarillo y diamantes.";
$AN437 = "Anillo en oro amarillo y diamantes.";
$AN443 = "Anillo en oro amarillo y diamantes.";
$AN445B = "Anillo en oro amarillo, diamantes y feldespato marfil.";
$AN465 = "Anillo en oro amarillo y diamantes.";
$AN487 = "Anillo en oro amarillo y diamantes.";
$AN540M = "Anillo en oro amarillo y diamantes.";
$AN562 = "Anillo en oro amarillo, diamantes y nefrita.";
$AN576 = "Anillo en oro amarillo, diamantes y jade rojo.";
$AN592B = "Anillo en oro amarillo, diamantes y cítricos.";
$AN607 = "Anillo en oro amarillo y diamantes.";
$AN620 = "Anillo en oro amarillo y diamantes.";
$AN623B = "Anillo en oro amarillo y diamantes.";
$AN720 = "Anillo en oro amarillo, diamantes y ónice.";
$AN720A = "Anillo de oro 18k y diamantes.";
$AN735 = "Anillo en oro amarillo.";
$AN735B = "Anillo EN ORO 18K.";
$AN757 = "Flyer - Anillo en oro amarillo y diamantes.";
$AN771 = "Chinatown - Anillo en oro amarillo y diamantes.";
$AN774 = "Flyer - Anillo en oro amarillo y diamantes.";
$AN778 = "Chinatown - Anillo en oro amarillo y diamantes.";
$AN780 = "Super Trees - Anillo en oro amarillo, diamantes y nefrita.";
$AN794 = "Anillo de oro 18k y diamantes.";
$AN796 = "Anillo de oro 18k y diamantes.";
$AN797 = "Anillo de oro 18k.";
$AN798A = "Anillo de oro 18k y diamantes.";
$AN798B = "Anillo en diamantes en oro amarillo 18k.";
$AN798C = "Anillo en diamantes en oro amarillo 18k.";
$AN799 = "Anillo de oro 18k y diamantes.";
$AN800 = "Anillo de oro 18k y diamantes.";
$AN801 = "Lenty - Anillo en diamantes en oro amarillo 18k.";
$AN799 = "Anillo de oro 18k y diamantes.";
$AN803 = "Catedral - Anillo en diamantes en oro amarillo 18k.";
$AN804 = "Faberge - Anillo en diamantes en oro amarillo 18k.";
$AN805 = "Moscovita - Anillo en diamantes en oro amarillo 18k.";
$AN807 = "Troika - Anillo en diamantes en oro amarillo 18k.";
$AN808 = "Bolshoi - Anillo en diamantes en oro amarillo 18k.";
$AN811 = "Czarina - Anillo en diamantes en oro amarillo 18k.";
$AN816 = "Anillo en diamantes en oro amarillo 18k.";
$AN827 = "Anillo en oro 18K con esmeralda y 10 diamantes de 0,5 pt.";
$AN832 = "Ipanema - Anillo en oro 18K con 15 diamantes de 1 pt.";
$AN834 = "Ipanema - Anillo en oro 18K con 15 diamantes de 1 pt.";
$AN835 = "Ipanema - Anillo en oro 18K con 14 diamantes de 1 pt y 1 diamante de 6 pts.";
$AN836 = "Copacabana - Anillo en oro 18K con 20 diamantes de 0,5 pt y 1 diamante de 6 pts.";
$AN837 = "Barra da Tijuca - Anillo en oro 18K con 20 diamantes de 0,5 pt y 2 diamantes de 2,5 pts.";
$AN841 = "Leblon - Anillo en oro 18K con 21 diamantes de 1 pt.";
$AN842 = "Copacabana - Anillo en oro 18K con 9 diamantes de 0,5 pt.";
$AN850 = "Leblon - Anillo en oro 18K con 48 diamantes de 0,5 pt.";
$AN857 = "Barra da Tijuca - Anillo en oro 18K con 16 diamantes de 0,5 pt.";
$AN859 = "Anillo EN ORO 18K.";
$AN860 = "Anillo EN ORO 18K.";
$AN868 = "Anillo en oro 18k con 12 diamantes 1,0 mm.";
$AN869 = "Anillo en oro 18k con 14 diamantes de 1 mm.";
$AN870 = "Anillo en oro 18k con 97 diamantes de 1 mm.";
$AN871 = "Anillo en oro 18k con 7 diamantes de 1mm.";
$AN873 = "Anillo en oro 18k con 7 diamantes 1,0 mm.";
$AN875 = "Anillo en oro 18k con 16 diamantes de 1 mm.";
$AN877 = "Anillo EN ORO 18K.";
$AN878 = "Anillo EN ORO 18K.";
$AN880 = "Anillo en oro 18k con 22 diamantes de 1 mm y gema redonda.";
$NA725 = "Anillo en oro amarillo y diamantes.";
$AN802 = "Samovar - Anillo en oro amarillo y diamantes.";
$AN765A= "Anillo de oro blanco con 42 diamantes (0,5 pt).";
$AN919 = "Anillo de oro amarillo con 48 diamantes (0,5 pt).";
$AN899 = "Anillo de oro amarillo con 21 diamantes (0,5 pt).";
$AN906 = "Anillo de oro amarillo con 11 diamantes (1 pt).";
$AN908 = "Anillo de oro amarillo con 32 diamantes (0,5 pt).";
$AN911 = "Anillo de oro amarillo con 4 diamantes (0,5 pt).";
$AN912 = "Anillo de oro amarillo con 18 diamantes (0,5 pt).";
$AN913 = "Anillo de oro amarillo con 34 diamantes (0,5 pt).";
$AN915 = "Anillo de oro amarillo con 21 diamantes (0,5 pt).";
$AN916 = "Anillo de oro amarillo con 24 diamantes (0,5 pt).";
$AN917 = "Anillo de oro amarillo con 18 diamantes (0,5 pt).";
$AN918 = "Anillo de oro amarillo con 19 diamantes (0,5 pt).";

$BR241 = "Pendiente en oro amarillo y diamantes.";
$BR346P = "Pendiente en oro amarillo y diamantes.";
$BR352 = "Pendiente en oro amarillo y diamantes y ágata blanca.";
$BR362C = "Pendiente en oro amarillo y diamantes.";
$BR363 = "Pendiente de oro 18k y diamantes.";
$BR366 = "Pendiente en oro 18k con 12 diamantes de 0,5 pt.";
$BR558 = "Pendiente en oro amarillo, diamantes y nefrita.";
$BR581 = "Pendiente de oro 18k, diamantes y ágata blanca.";
$BR600C = "Pendiente en oro amarillo y diamantes ahumado.";
$BR603 = "Pendiente en oro amarillo y diamantes.";
$BR606 = "Pendiente en oro amarillo y diamantes.";
$BR619 = "Pendiente en oro amarillo y diamantes.";
$BR755 = "Super Trees - Pendiente en oro amarillo y diamantes.";
$BR756 = "Litle India - Pendiente en oro amarillo, diamantes y nefrita.";
$BR757A = "Flyer - Pendiente en oro amarillo y diamante.";
$BR757B = "Pendiente de oro amarillo y diamantes.";
$BR767 = "Pendiente en oro amarillo y diamantes.";
$BR771 = "Litle India - Pendiente en oro amarillo y diamantes.";
$BR772 = "Litle India - Pendiente en oro amarillo y diamantes.";
$BR774 = "Pendiente en oro amarillo y diamantes.";
$BR775 = "Pendiente en oro amarillo y diamantes.";
$BR776 = "Litle India - Pendiente en oro amarillo y diamantes.";
$BR780 = "Super Trees - Pendiente en oro amarillo, diamantes y nefrita.";
$BR793 = "Pendiente en oro 18k con 26 diamantes de 1pt.";
$BR793B = "Pendiente en oro 18K con 24 bris de 1 pt.";
$BR794 = "Pendiente de oro 18k y diamantes.";
$BR798B = "Pendiente de oro 18k y diamantes.";
$BR798A = "Pendiente de oro 18k y diamantes.";
$BR798C = "Pendiente de oro 18k y diamantes.";
$BR799 = "Pendiente en oro 18k con 16 diamantes de 1 pt y 28 diamantes de 0,5 pt.";
$BR800 = "Pendiente de 18k y diamantes.";
$BR801 = "Lenty - Pendiente de oro 18k y diamantes.";
$BR803 = "Catedral - Pendiente de oro 18k y diamantes.";
$BR804 = "Faberge - Pendiente de oro 18k y diamantes.";
$BR805B = "Moscovita - Pendiente de oro 18k y diamantes.";
$BR806 = "Bolshoi - Pendiente de oro 18k y diamantes.";
$BR807 = "Troika - Pendiente de oro 18k y diamantes.";
$BR811 = "Czarina - Pendiente de oro 18k y diamantes.";
$BR814A = "Kandinsky - Pendiente de oro 18k y diamantes.";
$BR822 = "Pendiente en oro 18k.";
$BR826 = "Pendiente en oro 18k con esmeraldas y 28 diamantes de 0,5 pt.";
$BR827 = "Pendiente en oro 18k con 20 diamantes 0,5 pt y zafiros.";
$BR869 = "Pendiente en oro 18k con 26 diamantes de 1 mm.";
$BR872 = "Pendiente en oro 18k con 14 diamantes de 1 mm.";
$BR873 = "Pendiente en oro 18k con 40 diamantes de 1 mm y 2 diamantes de 1,30 mm.";
$BR875 = "Pendiente en oro 18k con 36 diamantes de 1 mm.";
$BR880 = "Pendiente en oro 18k con 54 diamantes de 1 mm y gema redonda.";
$BR805 = "Moscovita - Pendiente de oro 18k y diamantes.";
$BR899 = "Vicenza - Pendientes de oro amarillo con 22 diamantes (0,5 pt).";
$BR907 = "Vicenza - Pendientes de oro amarillo con 16 diamantes (0,5 pt).";
$BR911 = "Vicenza - Pendientes de oro amarillo con 8 diamantes (0,5 pt).";
$BR918 = "Vicenza - Pendientes de oro amarillo con 26 diamantes (0,5 pt).";

$C562 = "Conjunto de pendientes y collar en nefrita y diamantes en oro amarillo 18k.";
$C720 = "Conjunto de pendientes y Anillo en ónix y diamantes en oro amarillo 18k.";
$C757A = "Conjunto de pendientes y Anillo de diamantes en oro amarillo 18k.";
$C798C = "Conjunto de pendientes y Anillo de diamantes en oro amarillo 18k.";
$C800 = "Conjunto de pendientes y Anillo de diamantes en oro amarillo 18k.";
$Conjunto911 = "Conjunto de pendientes y Anillo de oro amarillo con 12 diamantes.";
$Conjunto918 = "Conjunto de pendientes y Anillo de oro amarillo con 45 diamantes.";
$Conjunto899 = "Conjunto de pendientes y Anillo de oro amarillo con 43 diamantes.";

$PI128 = "Pendiente en oro 18k.";
$PI429 = "Pendiente en oro amarillo y diamantes.";
$PI464 = "Pendiente en oro amarillo.";
$PI562 = "Pendiente en oro amarillo y diamantes, nefrita.";
$PI596 = "Pendiente en oro amarillo y diamantes.";
$PI771 = "Litle India - Pendiente en oro amarillo y diamantes.";
$PI776 = "Litle India - Pendiente en oro amarillo y diamantes.";
$PI798B = "Puesta en oro amarillo y diamantes.";
$PI832 = "Pendiente en oro 18k con 34 diamantes de 1,5 pt y 7 diamantes de 0,5 pt.";
$PI833 = "Puesta en oro 18k con 27 diamantes de 0,5pt. ";
$PI837 = "Pendiente en oro 18k con 24 diamantes de 0,5 pt.";
$PI838 = "Guanabara - Pendiente en oro 18k con 16 diamantes de 0,5 pt.";
$PI869 = "Pendiente en oro 18k con 22 diamantes de 1 mm.";
$PI871 = "Pendiente en oro 18k con 7 diamantes de 1 mm.";

$PUL81 = "Pulsera de 18 diamantes de 0,5 pt.";
$PUL19 = "Pulsera en oro amarillo y diamantes.";
$PUL17 = "Pulsera de oro amarillo con 28 diamantes (0,5 pt).";
$PUL494 = "Pulsera de oro amarillo con 18 diamantes (0,5 pt).";
$PUL798CA = "Pulsera de oro amarillo con 24 diamantes (0,5 pt).";

}


$lnkMenuSobre 	= "Sobre-A-GEO";
$lnkMenuContato = "Contato";

$lnkMenuSelva 	= "City-Lights";
$lnkMenuDunas	= "Dunas";
$lnkMenuRio		= "Rio-de-Janeiro";
$lnkMenuFleur	= "Fleur";
$lnkMenuMosc	= "Moscow";
$lnkMenuSing	= "Singapura";
$lnkMenuLima	= "Vicenza";

$lnkMenuAneis 		 = "Aneis";
$lnkMenuBrincos 	 = "Brincos";
$lnkMenuConjuntos 	 = "Conjuntos";
$lnkMenuGargantilhas = "Gargantilhas";
$lnkMenuPingentes 	 = "Pingentes";
$lnkMenuPulseiras 	 = "Pulseiras";

$lnkLancamento  = "Lancamentos";
