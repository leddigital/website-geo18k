<?php require('app/config/constants.php'); ?>
<?php require('app/control/Controller.php'); ?>

<html>
	<head>
		<title>GEO - Jóias 18K</title>
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="<?php echo $homeDesc; ?>" />
		<meta name="keywords" content="<?php echo $lnkMenuAneis; ?>, <?php echo $lnkMenuBrincos; ?>, <?php echo $lnkMenuConjuntos; ?>, <?php echo $lnkMenuGargantilhas; ?>, <?php echo $lnkMenuPingentes; ?>, <?php echo $lnkMenuPulseiras; ?>, <?php echo $lnkLancamento; ?>" />
<!-- 	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="web/js/skel.min.js"></script>
		<script src="web/js/init.js"></script>
 -->		

		<link rel="stylesheet" type="text/css" href="<?php echo $local; ?>web/font-awesome/font-awesome.min.css" />
		
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

		<link rel="stylesheet" href="<?php echo $local; ?>web/css/skel-noscript.css" />
		<link rel="stylesheet" href="<?php echo $local; ?>web/css/style.css" />
		<link rel="stylesheet" href="<?php echo $local; ?>web/css/style-desktop.css" />

		<link rel="stylesheet" type="text/css" href="<?php echo $local; ?>web/imgstyle/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $local; ?>web/imgstyle/css/demo.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $local; ?>web/imgstyle/css/set1.css" />

		<link rel="stylesheet" href="<?php echo $local; ?>web/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo $local; ?>web/datatable/datatables.min.css" />

		<link rel="shortcut icon" href="<?php echo $local; ?>web/images/favicon.png">

		<script
		  src="https://code.jquery.com/jquery-3.3.1.min.js"
		  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		  crossorigin="anonymous"></script>

		<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<script src="web/datatable/datatables.min.js"></script>

		<script type="text/javascript">
		jQuery(document).ready(function(){
		  
			jQuery("#ajax_form").submit(function(){
				if(document.getElementById("nome").value.length > 1){padding-top: 80px
					if(document.getElementById("email").value.length > 1){
						if(document.getElementById("textarea").value.length > 1){
							
						var dados = jQuery( this ).serialize();
						document.ajax_form.reset();
						
						jQuery.ajax({
							type: "POST",
							url: "app/config/post.php",
							data: dados,
							success: function( data )
							{
								alert( data );
							}
						});
						
						}else{
				  		alert("ESCREVA SUA MENSAGEM");
				  		}
					}else{
			  		alert("ESCREVA SEU EMAIL");
			  		}
				}else{
		  		alert("ESCREVA SEU NOME");
		  		}

				return false;
			});
		});
		
		
		</script>

	</head>
	<body class="homepage">

	<!-- Header -->
		<div id="header">

			<?php
			$useragent=$_SERVER['HTTP_USER_AGENT'];

			if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
				{
					require('app/view/menu-mobile.php');
				}
				else
				{
					require('app/view/menu-desktop.php');
				}
			?>	
		</div>
		

		<?php require($pagina); ?>
	

	<!-- Footer -->
		<?php require('app/view/footer.php'); ?>



<!-- 		<script>
			// For Demo purposes only (show hover effect on mobile devices)
			[].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
				el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			} );
		</script>
 -->
	</body>
</html>
